import React from 'react';
import AppBar from './components/AppBar'
import { Box, Button, Heading, Grommet } from 'grommet';
import { Notification } from 'grommet-icons';
import ProductList from './components/ProductList'
import ShoppingCartButton from './components/ShoppingCartButton'
import Nav from './components/Nav'

const theme = {
	global: {
		colors: {
			// brand: '#228BE6',
		},
		font: {
			family: 'Roboto',
			size: '16px',
			height: '15px',
		},
	},
};
	  
class App extends React.Component {

	state = {
		searchText: ''
	}

	handleSearch = (e) => {
		this.setState({
			searchText: e.target.value
		})
	}

	render() {
		return (
			<Grommet theme={theme} full>
				<Box fill>
					<AppBar>
						<Heading level='3' margin='none'>Shoe Store</Heading>
						<ShoppingCartButton/>
					</AppBar>
					<Box direction="row" pad="medium" fill>
						<Box width="medium">
							<Nav Search={this.handleSearch}/>
						</Box>
						<Box flex>
							<ProductList Search={this.state.searchText}/>
						</Box>
					</Box>
				 </Box>
				 
			</Grommet >
		);
	}
}

export default App;
