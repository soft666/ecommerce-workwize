import React from 'react'
import { Box, TextInput } from 'grommet'

class Nav extends React.Component {

    render() {
        const { Search } = this.props 
        return (
            <Box pad="small">
                <TextInput onChange={Search}></TextInput>
            </Box>
        )
    }
}

export default Nav