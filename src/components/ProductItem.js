import React from 'react'
import { Box, Stack, Image, Text, Button, Heading } from 'grommet'
import { Shop } from 'grommet-icons'
import { connect } from 'react-redux'

class ProductItem extends React.Component {

    render() {
        const { id, name, description, price, image } = this.props
        return (
            <Box direction="column" basis="medium" pad="small">
                <Box>
                    <Stack fill anchor="top-right">
                        <Box height="small">
                            <Image fit="cover" src={image} />
                        </Box>
                        <Box background="brand" pad="xsmall">
                            <Text>{price[0].amount}</Text>
                        </Box>
                    </Stack>
                </Box>
                <Box>
                    <Heading textAlign="start" level={5} margin={{vertical: 'xsmall'}}>
                        {name}
                    </Heading>
                    <Text textAlign="start" level={6}>
                        {description}
                    </Text>
                    <Button onClick={() => this.props.addItem(id)} primary pad="small" margin="small" icon={<Shop />} label="Add to cart"/>
                </Box>
            </Box>
        )
    }
}

const mapDispatch = ({cart: { addItem }}, { id }) => ({
    addItem: () => addItem(id)
})

export default connect(null, mapDispatch)(ProductItem)