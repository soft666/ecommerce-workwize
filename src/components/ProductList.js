import React from 'react'
import { Box } from 'grommet'
import request from '../utils/request'
import ProductItem from './ProductItem'

class ProductList extends React.Component {

	state = {
		data: [],
		searchText: ''
	}

	componentDidMount() {
		this.fetchData()
	}

	componentDidUpdate(prevProps, prevState) {
		if(prevProps.Search !== this.props.Search) {
			this.setState({
				searchText: this.props.Search
			})
			this.fetchData(this.props.Search)
		}
	}

	fetchData = async (text = '') => {
		const path = text !== '' ? `/products?filter=like(name,${text}*)` : '/products'
		const res = await request.get(path) 
		const data = res.data.data.map(item => {
			return {
				id: item.id,
				name: item.name,
				description: item.description,
				image: "https://via.placeholder.com/300x400.png",
				price: item.price
			}
		})
		this.setState({
			data,
		})
	}

	render() {
		const products = this.state.data
		return (
			<Box direction="column" pad="small" fill>
				<Box pad="small" background="light-3">
					ProductList
				</Box>
				<Box pad="small" direction="row" fill wrap overflow="auto">
					{
						products.map((product, key) => (<ProductItem Product {...product} key={key}></ProductItem>))
					}
				</Box>
			</Box>
		)
	}
		
}

export default ProductList