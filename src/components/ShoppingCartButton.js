import React from 'react'
import { connect } from 'react-redux'
import { Shop } from 'grommet-icons'
import { DropButton, Box, Stack } from 'grommet';

class ShoppingCartButton extends React.Component {

    render() {
        return (
            <DropButton dropAlign={{ top: 'bottom', right: 'right' }}
                dropContent={
                    <Box> 
                        <div>Cart product list</div>
                        <div>Cart product list</div>
                    </Box>
                }
            >
                <Stack anchor="top-right">
                    <Box pad='xsmall'>
                        <Shop size="32px"/>
                    </Box>
                    <Box
                        background="accent-1"
                        pad={{horizontal: 'xsmall'}}
                        round
                    >
                    {this.props.cartCount}
                    </Box>
                </Stack>
            </DropButton>
        )
    }
}

const mapState = state => {
    return {
        cartCount: state.cart.cartItems.length
    }
}

export default connect(mapState)(ShoppingCartButton)