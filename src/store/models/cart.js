export const cart = {
    state: {
        cartItems: [
            // {
            //     productId: 1,
            //     amount: 2
            // }
        ]
    }, // initial state
    reducers: {
        // handle state changes with pure functions
        addItem(state, payload) {

            if (state.cartItems.find(e => e.productId === payload) === undefined) {
                state = {
                    cartItems: [
                        ...state.cartItems, 
                        {
                            productId: payload,
                            amount: 0
                        }
                    ]
                }
            }
            state.cartItems.map(item => {
                if (item.productId === payload)
                    item.amount = item.amount + 1

                return item
            })

            return state
        },
        deleteItem(state, payload) {
            return state
        }
    },
    effects: (dispatch) => ({
        // handle state changes with impure functions.
        // use async/await for async actions
        //   async incrementAsync(payload, rootState) {
        //     await new Promise(resolve => setTimeout(resolve, 1000))
        //     dispatch.count.increment(payload)
        //   }
    })
  }